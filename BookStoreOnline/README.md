# BookStoreOnline

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.


BookStore Online requires Node.js 10.14.2 to run.


## Development server

Run `npm run start:proxy:mock:server` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


### Todos

 - Write MORE Tests
 - Add Docker

