import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  api = environment.apiUrl;

  constructor(private http: HttpClient) {}

  login(params) {
    return this.http.post(this.api + '/login', {
      email: params.email,
      password: params.password
    }).pipe(map((token: any) => {
      console.log(token)
      localStorage.setItem('token', JSON.stringify(token.accessToken));
    }));
  }

  logout() {
    localStorage.clear();
    window.location.reload();
  }

  register(params) {
    return this.http.post(this.api + '/users', {
      email: params.email,
      password: params.password
    });
  }

}
