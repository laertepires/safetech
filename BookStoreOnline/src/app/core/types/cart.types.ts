export class CartType {
  id: number;
  title: string;
  bookId: number;
  image: string;
  total: number;
}

export class CartsType {
  items: CartType[] = [];
  total: number = 0;
}
