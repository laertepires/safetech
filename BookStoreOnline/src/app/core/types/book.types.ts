export class BookType {
  id: number;
  title: string;
  author: string;
  image: string;
  description: string;
  value: number
}
