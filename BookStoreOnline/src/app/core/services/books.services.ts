import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class BooksService {

  api = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }


  getAllBooks() {
    return this.http.get(`${this.api}/books`);
  }

  getBookById(id) {
    return this.http.get(`${this.api}/books`, {
      params: {
        id
      }
    });
  }

  searchBookByTitle(title) {
    return this.http.get(`${this.api}/books?title_like=${title}`);
  }
}
