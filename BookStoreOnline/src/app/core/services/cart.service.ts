import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CartsType, CartType } from '../types/cart.types';


@Injectable({
  providedIn: 'root'
})

export class CartService {

  api = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getCart() {
    return this.http.get(`${this.api}/carts`).pipe(map((data: any) => {
      let carts = new CartsType();
      carts.total = data.length;
  
      data.forEach(element => {
        let cart = new CartType();
        cart = element;
        carts.items.push(cart);
      });
      return carts;
    }));
  }

  addToCart(payload) {
    return this.http.post(`${this.api}/carts`, payload);
  }

  updateCart(id: number, payload) {
    return this.http.put(`${this.api}/carts/${id}`, payload);
  }
}
