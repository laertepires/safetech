import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BooksService } from 'src/app/core/services/books.services';
import {  BookType } from 'src/app/core/types/book.types';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { CartService } from './../../core/services/cart.service';
import { EventDispatcher } from './../../shared/EventDispatcher';
import Events from './../../shared/events';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BooksComponent implements OnInit {

  /**
   * array books
   */
  books: BookType[] = [];

  /**
   * form
   */
  form: FormGroup;

  /**
   * slider config
   */

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  slideConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };

  constructor(
    public service: BooksService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private cartService: CartService
  ) { }

  /**
   * init from component
   */
  ngOnInit(): void {

    this.form = this.formBuilder.group({
      search: ['']
    });

    this.service.getAllBooks().subscribe((data: BookType[]) => {
      this.books = data;
    });
  }

  /**
   * submit search
   */
  onSubmit() {
    this.service.searchBookByTitle(this.form.get('search').value).subscribe((data: BookType[]) => {
      this.books = data;
    });
  }

  addToCart(book: BookType) {
    const payload: any = {};
    payload.bookId = book.id;
    payload.title = book.title;
    payload.image = book.image;
    payload.value = book.value

    this.cartService.addToCart(payload).subscribe(data => {
      this.snackBar.open('Livro adicionado ao carrinho', 'Undo', {
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      EventDispatcher.dispatch(Events.Cart.Update);
    });
  }
}
