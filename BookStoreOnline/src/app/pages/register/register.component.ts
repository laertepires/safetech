import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  success = false;
  failed = false;
  submited: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', Validators.required ],
      password: ['', Validators.required]
    });
  }

  getField(control) {
    return this.form.get(control);
  }

  onSubmit() {
    this.submited = true;
    if(this.form.valid) {
      this.authService.register(this.form.value).subscribe(() => {
        this.success = true;
        this.failed = false;
      }, error => {
        this.failed = true;
        this.success = false;
      });
    }
  }

  goToRegister() {
    this.router.navigate(['/login']);
  }

}
