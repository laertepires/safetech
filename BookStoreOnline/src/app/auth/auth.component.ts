import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
// import { TokenStorageService } from '../services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  isLoginFailed: boolean;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }


  ngOnInit(): void {

    this.form = this.formBuilder.group({
      email: ['', Validators.required ],
      password: ['', Validators.required]
    });

  }

  getField(control) {
    return this.form.get(control);
  }

  onSubmit() {
    if(this.form.valid) {
      this.authService.login(this.form.value).subscribe(() => {
        this.isLoginFailed = true;
        this.router.navigate(['/list']);
      }, error => {
        console.log(error);
      });
    }
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }
}
