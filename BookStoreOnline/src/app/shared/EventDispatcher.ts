import { EventEmitter } from 'events';

export class EventDispatcher {

	static event = new EventEmitter();

	static dispatch(myEventName: string, data = null) {
		this.event.emit(myEventName, data);
	}

	static listen(myEventName: string, handler: any) {
		this.event.addListener(myEventName, handler);
	}

	static removeListener(myEventName: string) {
		this.event.removeAllListeners(myEventName);
	}

	constructor() {

	}
}
