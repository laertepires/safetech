import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookType } from 'src/app/core/types/book.types';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})

export class BookCardComponent implements OnInit {

  @Input() book: BookType;
  @Output() addEmit = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  rent() {
    if (!this.book.available) {
      this.addEmit.emit(this.book);
    }
  }

}
