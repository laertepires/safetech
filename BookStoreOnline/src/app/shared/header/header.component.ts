import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from './../../core/services/cart.service';
import Events from './../events';
import { EventDispatcher } from '../EventDispatcher';
import { CartsType } from 'src/app/core/types/cart.types';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  opened = false;
  cart : CartsType;

  constructor(
    private authService: AuthService,
    private bookService: CartService
    ) { }

  ngOnInit(): void {
    this.getCart();

    EventDispatcher.listen(Events.Cart.Update, () => {
			this.getCart();
    });
  }

  openClose() {
    if(this.opened) {
      this.animateRight();
    } else {
      this.animateLeft();
    }
    this.opened = !this.opened;
  }

  animateLeft() {
    document.getElementById("summary").style.right = '0';
  }

  animateRight() {
    document.getElementById("summary").style.right = '-500px';
  }

  getCart() {
    this.bookService.getCart().subscribe((data: CartsType) => {
      this.cart = data
      console.log(data);
    });
  }
  logout() {
    this.authService.logout();
  }

  ngOnDestroy() {
		EventDispatcher.removeListener(Events.Cart.Update);
	}
}
